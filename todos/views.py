from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm

# Create your views here.


def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {
        "todolistdetail": todolist,
    }
    return render(request, "todos/todo_detail.html", context)


def todolist(request):
    list = TodoList.objects.all()
    context = {"todolist": list}
    return render(request, "todos/todos.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form = form.save()

            return redirect("todo_list_detail", form.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def todo_list_edit(request, id):
    list = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=list)
    context = {
        "list_object": list,
        "form": form,
    }
    return render(request, "todos/edit.html", context)
