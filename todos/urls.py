from django.urls import path
from todos.views import todolist, todo_list_detail, todo_list_create
from todos.views import todo_list_edit

urlpatterns = [
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("", todolist, name="todo_list_list"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("edit/<int:id>/", todo_list_edit, name="todo_list_update"),
]
